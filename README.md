<table>
    <tr><td><strong>blender.chat</strong></td><td><a href='https://blender.chat/direct/HooglyBoogly'>HooglyBoogly</a></td></tr>
    <tr><td><strong>Email</strong></td><td>hans@blender.org</td></tr>
    <tr><td><strong>Mastodon</strong></td><td><a href='https://mastodon.social/@hansgoudey'>@hansgoudey@mastodon.social</a></td></tr>
    <tr><td><strong>Weekly Reports</strong></td><td>
<a href='https://projects.blender.org/HooglyBoogly/.profile/src/branch/main/reports/2025.md'>2025</a>
/ <a href='https://projects.blender.org/HooglyBoogly/.profile/src/branch/main/reports/2024.md'>2024</a>
/ <a href='https://wiki.blender.org/wiki/User:HooglyBoogly/Reports/2023'>2023</a>
/ <a href='https://wiki.blender.org/wiki/User:HooglyBoogly/Reports/2022'>2022</a>
/ <a href='https://wiki.blender.org/wiki/User:HooglyBoogly/Reports/2021'>2021</a>
/ <a href='https://wiki.blender.org/wiki/User:HooglyBoogly/Reports/2020'>2020</a>
    </td></tr>
</table>



Hi! I'm a Blender developer employed by the Blender Foundation. So far I've mainly worked on the user interface, the geometry nodes system, geometry processing and storage, and sculpting.